import {Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {select, Store} from '@ngrx/store';
import {WeatherActions} from '../store/actions/weather.actions';
import {GeolocationStore, WeatherStore} from '../store/reducers/index';
import {Subject} from 'rxjs/index';
import {takeUntil} from 'rxjs/internal/operators';
import {ApiCallService} from './api-call.service';
import {AppState} from '../store/reducers/reducers';
import {GeoActions} from '../store/actions/geolocation.actions';
import {ViewService} from './view.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {

  private ngUnsubscribe$: Subject<void> = new Subject<void>();
  temp = [];
  city = '';
  closest = {
    temp: 0
  }
  colorScheme = {
    domain: []
  };
  loading = true;
  type = '';
  @ViewChild('template') template: TemplateRef<any>;

  constructor(private store: Store<AppState>, private api: ApiCallService, private vs: ViewService) {
  }

  ngOnInit(): void {
    this.vs.getLocation()
      .subscribe(pos => {
        this.store.dispatch(new GeoActions.GetGeoData(pos.lat, pos.lng));
        this.store.pipe(
          select(GeolocationStore.getGeoState),
          takeUntil(this.ngUnsubscribe$),
        ).subscribe(res => {
          if (res && res.locations.length) {
            this.city = res.locations[0].address_components[3].long_name;
             this.store.dispatch(new WeatherActions.GetWeatherData(this.city));
            this.store.pipe(
              select(WeatherStore.getWeatherState),
              takeUntil(this.ngUnsubscribe$)
            ).subscribe(reso => {
              if (reso && reso.weathers.length) {
                this.type = reso.weathers[0].weather[0].main;
                console.log(reso);
                this.loading = reso.loading;
                this.closest.temp = reso.weathers[0].main.temp;
                this.temp = reso.weathers.map(elm => {
                  const time = new Date(elm.dt * 1000);
                  return {
                    name: `${time.getHours() < 10 ? `0${time.getHours()}` : time.getHours()}:${time.getMinutes() < 10 ? `${time.getMinutes()}0` : time.getMinutes()}`,
                    value: elm.main.temp
                  };
                });
                console.log(this.temp);
                this.colorScheme.domain = new Array<string>(this.temp.length).fill('#43a047');
                this.temp.forEach((elem, index) => {
                  if (elem.value > 20) {
                    this.colorScheme.domain[index] = '#e53935';
                  }
                });
              }
            });
          }
        });
      });
  }
  ngOnDestroy(): void {
    this.ngUnsubscribe$.next();
    this.ngUnsubscribe$.complete();
  }
}
