import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiCallService {

  constructor(private http: HttpClient) {
  }

  getCityName(lat: number, lng: number) {
    return this.http.get(`${environment.geoCoding}${lat},${lng}&key=${environment.key}`);
  }

}
