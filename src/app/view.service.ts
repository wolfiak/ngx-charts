import {Injectable, OnDestroy} from '@angular/core';
import {Observable, Subject} from 'rxjs/index';
import {GeolocationStore, WeatherStore} from '../store/reducers';
import {WeatherActions} from '../store/actions/weather.actions';
import {takeUntil} from 'rxjs/internal/operators';
import {GeoActions} from '../store/actions/geolocation.actions';
import {select, Store} from '@ngrx/store';
import {ApiCallService} from './api-call.service';
import {AppState} from '../store/reducers/reducers';

@Injectable({
  providedIn: 'root'
})
export class ViewService  {
  private _city = '';
  private temp;
  private colorScheme;
  private closest;
  private ngUnsubscribe$: Subject<void> = new Subject<void>();

  constructor(private store: Store<AppState>, private api: ApiCallService) {
  }

  get city() {
    return this._city;
  }

  getLocation(): Observable<any> {
    return new Observable<any>(observer => {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(pos => {
          const location = {
            lng: pos.coords.longitude,
            lat: pos.coords.latitude
          }
          observer.next(location);
        });
      } else {
        observer.complete();
      }
    });
  }
}
