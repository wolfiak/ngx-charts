import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import {StoreModule} from '@ngrx/store';
import {reducers} from '../store/reducers/reducers';
import {HttpClientJsonpModule, HttpClientModule} from '@angular/common/http';
import {EffectsModule} from '@ngrx/effects';
import {WeatherEffects} from '../store/effects/weather.effects';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {FormsModule} from '@angular/forms';
import {MatCardModule} from '@angular/material/card';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import {MatInputModule} from '@angular/material/input';
import {GeoEffect} from '../store/effects/geolocation.effect';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {ViewService} from './view.service';
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    StoreModule.forRoot(reducers),
    EffectsModule.forRoot([WeatherEffects, GeoEffect]),
    BrowserModule,
    HttpClientModule,
    HttpClientJsonpModule,
    FormsModule,
    NgxChartsModule,
    BrowserAnimationsModule,
    MatCardModule,
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production }),
    MatInputModule,
    MatProgressSpinnerModule
  ],
  providers: [ViewService],
  bootstrap: [AppComponent]
})
export class AppModule { }
