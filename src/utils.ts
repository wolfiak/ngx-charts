export function isPrimitive(value: any): boolean {
  switch (typeof value) {
    case 'string':
    case 'number':
    case 'boolean':
      return true;
  }
  return (value instanceof String || value === String ||
    value instanceof Number || value === Number ||
    value instanceof Boolean || value === Boolean);
}

/**
 * Checks if provided value is array.
 * @param {any} value
 * @returns {boolean}
 */
export function isArray(value: any): boolean {
  if (value === Array) {
    return true;
  } else if (typeof Array.isArray === 'function') {
    return Array.isArray(value);
  } else {
    return value instanceof Array;
  }
}

/**
 * This function coerces a string into a string literal type.
 * Using tagged union types in TypeScript 2.0, this enables
 * powerful typechecking of our store.
 *
 * Since every action label passes through this function it
 * is a good place to ensure all of our action labels
 * are unique.
 */
const typeCache: { [label: string]: boolean } = {};

export function type<T>(label: T | ''): T {
  if (typeCache[<string>label]) {
    throw new Error(`Action type "${label}" is not unique"`);
  }

  typeCache[<string>label] = true;

  return <T>label;
}

export function countIt(stringo) {
  const array = stringo.split('');
  return array.reduce((acc, elm) => {
    if (elm === ' ') {
      return null;
    } else {
      return acc + 1;
    }
  }, 0);
}

export function Trimier(stringo) {
  const array = stringo.split('');
  return array.reduce((acc, elm) => {
    if (elm === ' ') {
      return acc;
    } else {
      return acc + elm;
    }
  }, '');
}


