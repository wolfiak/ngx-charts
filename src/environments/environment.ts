// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  api: 'e4fb3471cb354af46cb7929d91decd51',
  endpoint: '//api.openweathermap.org/data/2.5/forecast?q=',
  geoCoding: 'https://maps.googleapis.com/maps/api/geocode/json?latlng=',
  key: 'AIzaSyD1OV8CaILb3JEsgsqgs9qB1y4tPis1rLQ'
};
