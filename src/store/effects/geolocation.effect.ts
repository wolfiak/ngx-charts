import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {ApiCallService} from '../../app/api-call.service';
import {GeoActions} from '../actions/geolocation.actions';
import {map, switchMap} from 'rxjs/internal/operators';


@Injectable()
export class GeoEffect {
  constructor(private actions$: Actions, private api: ApiCallService) {
  }

  @Effect()
  getLocation$ = this.actions$
    .pipe(
      ofType(GeoActions.types.getGeoData),
      switchMap((res) => {
        return this.api.getCityName((<GeoActions.GetGeoData>res).lat, (<GeoActions.GetGeoData>res).lng)
          .pipe(
            map((coord: any) => {
              console.log('coord effect:', coord);
              return new GeoActions.GetGeoDataSuccess(coord.results);
            })
          );
      })
    );
}
