import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {WeatherActions} from '../actions/weather.actions';
import {map, switchMap} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';


@Injectable()
export class WeatherEffects {
  constructor(private actions$: Actions, private http: HttpClient) {
  }

  @Effect()
  fetchWeather$ = this.actions$
    .pipe(
      ofType(WeatherActions.types.getWeatherData),
      switchMap((payload) => {
        return this.http.get(`${environment.endpoint}${(<WeatherActions.GetWeatherData>payload).city},pl&APPID=${environment.api}&units=metric`).pipe(
          map((res: any) => {
            return new WeatherActions.GetWeatherDataSuccess(res.list);
          }));
      })
    );
}
