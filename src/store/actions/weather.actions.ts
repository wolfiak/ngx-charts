import * as Utils from '../../utils';
import {Action} from '@ngrx/store';


export namespace WeatherActions {

  export const types = {
    getWeatherData: Utils.type('[Document] Get weather data'),
    getWeatherDataSuccess: Utils.type('[Document] Get weather data Success'),
    getWeatherDataFailure: Utils.type('[Document] Get weather data Failure')
  };

  export class GetWeatherData implements Action {
    readonly type = types.getWeatherData;

    constructor(public city: string) {
    }
  }

  export class GetWeatherDataSuccess implements Action {
    readonly type = types.getWeatherDataSuccess;

    constructor(public payload: any) {
    }
  }

  export class GetWeatherDataFailure implements Action {
    readonly type = types.getWeatherDataFailure;

    constructor(public payload: string) {
    }
  }

  export type All = GetWeatherData | GetWeatherDataSuccess | GetWeatherDataFailure;
}
