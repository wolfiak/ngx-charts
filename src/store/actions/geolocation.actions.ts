import * as Utils from '../../utils';
import {Action} from '@ngrx/store';


export namespace GeoActions {

  export const types = {
    getGeoData: Utils.type('[Document] Get Geo data'),
    getGeoDataSuccess: Utils.type('[Document] Get Geo data Success'),
    getGeoDataFailure: Utils.type('[Document] Get Geo data Failure')
  };

  export class GetGeoData implements Action {
    readonly type = types.getGeoData;

    constructor(public lat: number, public lng: number) {
    }
  }

  export class GetGeoDataSuccess implements Action {
    readonly type = types.getGeoDataSuccess;

    constructor(public payload: any) {
    }
  }

  export class GetGeoDataFailure implements Action {
    readonly type = types.getGeoDataFailure;

    constructor(public payload: string) {
    }
  }

  export type All = GetGeoData | GetGeoDataSuccess | GetGeoDataFailure;
}
