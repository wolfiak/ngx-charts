import {weatherReducer, WeatherState} from './weather.reducer';
import {ActionReducerMap} from '@ngrx/store';
import {geoReducer, GeoState} from './geolocation.reducer';


export interface AppState {
  weatherReducer: WeatherState;
  geoReducer: GeoState;
}

export const reducers: ActionReducerMap<AppState> = {
  weatherReducer: weatherReducer,
  geoReducer: geoReducer
}
