import {WeatherActions} from '../actions/weather.actions';

export type Action = WeatherActions.All;

export interface WeatherState {
  error: string;
  loading: boolean;
  weathers: any[];
}

const weatherState: WeatherState = {
  error: '',
  loading: false,
  weathers: []
}

export function weatherReducer(state: WeatherState = weatherState, action: Action) {
  switch (action.type) {
    case WeatherActions.types.getWeatherData: {
      return {
        ...state,
        loading: true
      };
    }
    case WeatherActions.types.getWeatherDataSuccess: {
      return {
        ...state,
        loading: false,
        weathers: (<WeatherActions.GetWeatherDataSuccess>action).payload
      };
    }
    case WeatherActions.types.getWeatherDataFailure: {
      return {
        ...state,
        loading: false,
        error: (<WeatherActions.GetWeatherDataFailure>action).payload
      };
    }
  }
}
