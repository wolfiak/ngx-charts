import {GeoActions} from '../actions/geolocation.actions';

export type Action = GeoActions.All;

export interface GeoState {
  error: string;
  loading: boolean;
  locations: any[];
}

const geoState: GeoState = {
  error: '',
  loading: false,
  locations: []
}

export function geoReducer(state: GeoState = geoState, action: Action) {
  switch (action.type) {
    case GeoActions.types.getGeoData: {
      return {
        ...state,
        loading: true
      };
    }
    case GeoActions.types.getGeoDataSuccess: {
      return {
        ...state,
        loading: false,
        locations: (<GeoActions.GetGeoDataSuccess>action).payload
      };
    }
    case GeoActions.types.getGeoDataFailure: {
      return {
        ...state,
        loading: false,
        error: (<GeoActions.GetGeoDataFailure>action).payload
      };
    }
  }
}
