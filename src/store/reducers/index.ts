import {createFeatureSelector, createSelector} from '@ngrx/store';
import {WeatherState} from './weather.reducer';
import {GeoState} from './geolocation.reducer';

export namespace WeatherStore {
  export const getWeatherModuleState = createFeatureSelector<WeatherState>('weatherReducer');
  export const getWeatherState = createSelector(
    getWeatherModuleState,
    state => state
  );
}
export namespace GeolocationStore {
  export const getGeolocationModuleState = createFeatureSelector<GeoState>('geoReducer');
  export const getGeoState = createSelector(
    getGeolocationModuleState,
    state => state
  );
}
